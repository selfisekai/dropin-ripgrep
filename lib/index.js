'use strict';

const which = require('which');

module.exports.rgPath = which.sync('rg', {nothrow: true}) || which.sync('ripgrep', {nothrow: true}) || '/usr/bin/rg';
